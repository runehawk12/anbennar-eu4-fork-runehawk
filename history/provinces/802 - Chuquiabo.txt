#802

culture = green_orc
religion = great_dookan
capital = ""
tribal_owner = B43
hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = unknown

native_size = 30
native_ferocity = 8
native_hostileness = 8
latent_trade_goods = {
	coal
}